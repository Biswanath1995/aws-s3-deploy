#!/usr/bin/env bash
#
# Deploy to AWS S3, http://aws.amazon.com/s3/
#
# Required globals:
#   AWS_ACCESS_KEY_ID
#   AWS_SECRET_ACCESS_KEY
#   AWS_DEFAULT_REGION
#   S3_BUCKET
#   LOCAL_PATH
#
# Optional globals:
#   CONTENT_ENCODING
#   ACL
#   STORAGE_CLASS
#   CACHE_CONTROL
#   EXPIRES
#   DELETE_FLAG
#   EXTRA_ARGS
#   DEBUG

source "$(dirname "$0")/common.sh"

# mandatory parameters
S3_BUCKET=${S3_BUCKET:?'S3_BUCKET variable missing.'}
LOCAL_PATH=${LOCAL_PATH:?'LOCAL_PATH variable missing.'}


default_authentication() {
  info "Using default authentication with AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY."
  AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID:?'AWS_ACCESS_KEY_ID variable missing.'}
  AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY:?'AWS_SECRET_ACCESS_KEY variable missing.'}
}

oidc_authentication() {
  info "Authenticating with a OpenID Connect (OIDC) Web Identity Provider."
      mkdir -p /.aws-oidc
      AWS_WEB_IDENTITY_TOKEN_FILE=/.aws-oidc/web_identity_token
      echo "${BITBUCKET_STEP_OIDC_TOKEN}" >> ${AWS_WEB_IDENTITY_TOKEN_FILE}
      chmod 400 ${AWS_WEB_IDENTITY_TOKEN_FILE}
      aws configure set web_identity_token_file ${AWS_WEB_IDENTITY_TOKEN_FILE}
      aws configure set role_arn ${AWS_OIDC_ROLE_ARN}
      unset AWS_ACCESS_KEY_ID
      unset AWS_SECRET_ACCESS_KEY
}

setup_authentication() {
  enable_debug
  AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION:?'AWS_DEFAULT_REGION variable missing.'}
  if [[ -n "${AWS_OIDC_ROLE_ARN}" ]]; then
    if [[ -n "${BITBUCKET_STEP_OIDC_TOKEN}" ]]; then
      oidc_authentication
    else
      warning 'Parameter `oidc: true` in the step configuration is required for OIDC authentication'
      default_authentication
    fi
  else
    default_authentication
  fi
}

setup_authentication

preexecution_hook() {
  if [[ -n "${PRE_EXECUTION_SCRIPT}" ]]; then
    if [[ ! -f "${PRE_EXECUTION_SCRIPT}" ]]; then
      fail "$PRE_EXECUTION_SCRIPT preexecution hook file doesn't exist."
    fi
    ./$PRE_EXECUTION_SCRIPT
  fi
}


preexecution_hook

if [[ ! -d "${LOCAL_PATH}" ]]; then
  fail "$LOCAL_PATH directory doesn't exist."
fi

# default parameters
EXTRA_ARGS=${EXTRA_ARGS:=""}

AWS_DEBUG_ARGS=""
if [[ "${DEBUG}" == "true" ]]; then
  info "Enabling debug mode."
  AWS_DEBUG_ARGS="--debug"
fi


declare -A AWS_S3_ARGS=()
AWS_S3_ARGS["cache-control"]=${CACHE_CONTROL}
AWS_S3_ARGS["content-encoding"]=${CONTENT_ENCODING}
AWS_S3_ARGS["acl"]=${ACL}
AWS_S3_ARGS["expires"]=${EXPIRES}
AWS_S3_ARGS["storage-class"]=${STORAGE_CLASS}

# Build the arguments string
ARGS_STRING=""

for key in "${!AWS_S3_ARGS[@]}"
do
  if [ -n "${AWS_S3_ARGS[$key]}" ]; then
    ARGS_STRING+="--$key=${AWS_S3_ARGS[$key]} "
  fi
done

if [[ "${DELETE_FLAG}" == "true" ]]; then
  ARGS_STRING+="--delete "
fi

ARGS_STRING+="${EXTRA_ARGS}"

info "Starting deployment to S3..."
run aws s3 sync ${LOCAL_PATH} s3://${S3_BUCKET}/ ${ARGS_STRING[@]} ${AWS_DEBUG_ARGS}
if [[ "${status}" -eq 0 ]]; then
  success "Deployment successful."
else
  fail "Deployment failed."
fi
