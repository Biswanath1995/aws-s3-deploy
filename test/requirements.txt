pytest==6.*
flake8==3.7
boto3==1.17.*

bitbucket-pipes-toolkit==3.0.0
